int leftBrush=0;
int rightBrush=0x20;
int leftColor=0x0;
int rightColor=0x0;
byte[] base;
boolean colorBrush=true;
boolean charBrush=true;
boolean leftSide=false;
Screen c64;
PImage[] asciiFont;
String name="default";
String allowedKeys=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKLMNOPQRSTUVWXYZ0123456789.-_";
boolean nameFieldMarked=false;
int downCount=0;

void setup(){
  base=loadBytes("data/base");
  size(1024,512);
  background(0);
  c64=new Screen();
  loadAscii();
  c64.loadPic("pics/default.prg");
  c64.render(0,0,16);
  for(int i=0;i<16;i++){
    c64.drawEmpty(47+i,18,i);
    c64.drawEmpty(47+i,20,i);
  }
  tint(255,255,255,255);
  type("chr[ ]",768,368);
  type("col[ ]",896,368);
  type("Name:",752,400);
  tint(255,255,0,255);
  type("[Save]",752,448);
  type("[Load]",912,448);
}

void type(String input,int x,int y){
  for(int i=0;i<input.length();i++)
    image(asciiFont[int(input.charAt(i))],x+i*16,y,16,16);
}

void draw(){
  drawCharPalette();
  downCount--;
}

void keyTyped() {
  if(nameFieldMarked){
    boolean allowed=false;
    for(int i=0;i<allowedKeys.length();i++)
      if(allowedKeys.charAt(i)==key&&name.length()<13)
        name+=key;
    if(key==BACKSPACE&&name.length()>0)
      name=name.substring(0,name.length()-1);
    if(key==ENTER)
      nameFieldMarked=false;
    loop();
  }
}

void drawCharPalette(){
  if(rightBrush==leftBrush){
    fill(255,255,0);
    rect(752+(rightBrush&0xf)*16,16+(rightBrush>>4)*16,16,16);
  } else {
    fill(0,255,0);
    rect(752+(rightBrush&0xf)*16,16+(rightBrush>>4)*16,16,16);
    fill(255,0,0);
    rect(752+(leftBrush&0xf)*16,16+(leftBrush>>4)*16,16,16);
  }
  fill(0,0,0);
  for(int y=0;y<16;y++)
    for(int x=0;x<16;x++){
      if((x+(y<<4))!=leftBrush&&(x+(y<<4))!=rightBrush)
        rect(752+16*x,16+16*y,16,16);
      c64.drawChar(47+x,1+y,x+(y<<4),0xffffffff);
    }
  for(int i=0;i<16;i++){
    c64.drawEmpty(47+i,19,0);
    c64.drawEmpty(47+i,21,0);
  }
  if(leftColor==rightColor){
    tint(255,255,0);
    image(asciiFont[0x7c],752+leftColor*16,304,16,16);
  } else {
    tint(255,0,0);
    image(asciiFont[0x7c],752+leftColor*16,304,16,16);
    tint(0,255,0);
    image(asciiFont[0x7c],752+rightColor*16,304,16,16);
  }
  if(c64.getBorder()==c64.getBackground()){
    tint(255,255,0);
    image(asciiFont[0x7c],752+c64.getBackground()*16,336,16,16);
  } else {
    tint(255,0,0);
    image(asciiFont[0x7c],752+c64.getBackground()*16,336,16,16);
    tint(0,255,0);
    image(asciiFont[0x7c],752+c64.getBorder()*16,336,16,16);
  }
  tint(255,255,255,255);
  if(charBrush)
    image(asciiFont[int('+')],832,368,16,16);
  else
    image(asciiFont[int(' ')],832,368,16,16);
  if(colorBrush)
    image(asciiFont[int('+')],960,368,16,16);
  else
    image(asciiFont[int(' ')],960,368,16,16);
  if(nameFieldMarked)
    tint(255,0,0,255);
  else tint(255,255,255,255);
  image(asciiFont[int('[')],752,416,16,16);
  image(asciiFont[int(']')],992,416,16,16);
  for(int i=0;i<14;i++)
    if(i<name.length())
      image(asciiFont[int(name.charAt(i))],768+16*i,416,16,16);
    else if(i==name.length()){
      if(nameFieldMarked)
        image(asciiFont[int('_')],768+16*i,416,16,16);
      else
        image(asciiFont[int(' ')],768+16*i,416,16,16);
    } else if(i>name.length())
        image(asciiFont[int(' ')],768+16*i,416,16,16);
  if(downCount<=0) {
    type("                ",752,484);
    noLoop();
  }
}

void mousePressed(){
  if(mouseX>=736)
    leftSide=false;
  else leftSide=true;
  if(mouseX>=752&&mouseX<1008&&mouseY>=416&&mouseY<432)
    nameFieldMarked=true;
  else nameFieldMarked=false;
  if(mouseX>=752&&mouseX<848&&mouseY>=448&&mouseY<464)
    c64.savePic("pics/" + name.toLowerCase() + ".prg");
  if(mouseX>=912&&mouseX<1008&&mouseY>=448&&mouseY<464)
    c64.loadPic("pics/" + name.toLowerCase() + ".prg");
  
//  type("[Save]",752,448);
//  type("[Load]",912,448);
  mouseCheck();
}

void mouseDragged(){
  mouseCheck();
}

void mouseCheck(){
  if(leftSide){
    if(mouseX>=48&&mouseX<688&&mouseY>=48&mouseY<448)
      if(mouseButton==LEFT){
        if(colorBrush&&charBrush)c64.setCell(((mouseX-48)>>4),((mouseY-48)>>4),leftBrush,leftColor);
        else if(charBrush)c64.setChar(((mouseX-48)>>4),((mouseY-48)>>4),leftBrush);
        else if(colorBrush)c64.setColor(((mouseX-48)>>4),((mouseY-48)>>4),leftColor); 
      }
      else if(mouseButton==RIGHT){
        if(colorBrush&&charBrush)c64.setCell(((mouseX-48)>>4),((mouseY-48)>>4),rightBrush,rightColor);
        else if(charBrush)c64.setChar(((mouseX-48)>>4),((mouseY-48)>>4),rightBrush);
        else if(colorBrush)c64.setColor(((mouseX-48)>>4),((mouseY-48)>>4),rightColor);
      }
  } else {
    if(mouseX>=752&&mouseX<1008&&mouseY>=16&&mouseY<272)
      if(mouseButton==LEFT)
        leftBrush=((mouseX-752)>>4)+(((mouseY-16)>>4)<<4);
      else if(mouseButton==RIGHT)
        rightBrush=((mouseX-752)>>4)+(((mouseY-16)>>4)<<4);
    if(mouseX>=752&&mouseX<1008&&mouseY>=288&&mouseY<320)
      if(mouseButton==LEFT)
        leftColor=(mouseX-752)>>4;
      else if(mouseButton==RIGHT)
        rightColor=(mouseX-752)>>4;
    if(mouseX>=752&&mouseX<1008&&mouseY>=320&&mouseY<352)
      if(mouseButton==LEFT)
        c64.setBackground((mouseX-752)>>4);
      else if(mouseButton==RIGHT)
        c64.setBorder((mouseX-752)>>4);         
    if(mouseX>=832&&mouseX<848&&mouseY>=368&&mouseY<384)
      if(mouseButton==LEFT)
        charBrush=true;
      else if(mouseButton==RIGHT)
        charBrush=false; 
    if(mouseX>=960&&mouseX<976&&mouseY>=368&&mouseY<384)
      if(mouseButton==LEFT)
        colorBrush=true;
      else if(mouseButton==RIGHT)
        colorBrush=false;
  }
  loop();
}

void loadAscii(){
  PImage ascii=loadImage("data/ascii.png");
  asciiFont=new PImage[256];
  for(int i=0;i<256;i++)
    asciiFont[i]=ascii.get((i&0xf)*8,(i>>4)*8,8,8);
}

class Screen {
  byte[] chars;
  byte[] colors;
  byte borderColor=0x0e;
  byte backgroundColor=0x6;
  PImage paletteImage;
  int[] palette;
  PImage[] font;
  PImage empty;
  
  public Screen(){
    chars=new byte[1000];
    colors=new byte[1000];
    paletteImage=loadImage("data/palette.png");
    palette = new int[16];
    for(int i=0;i<16;i++)
      palette[i]=paletteImage.get(i,0)|0xff000000;
    font=new PImage[256];
    convertFont();
    //loadScreen("default");
    empty=new PImage(8,8);
    empty.loadPixels();
    for(int i=0;i<64;i++)
      empty.pixels[i]=0xffffffff;
    empty.updatePixels();
  }
  public void setChar(int x,int y, int value){
    if(chars[40*y+x]!=byte(value&0xff)){
      chars[40*y+x]=byte(value&0xff);
      tint(palette[backgroundColor]);
      image(empty,x*16+48,y*16+48,16,16);
      tint(palette[int(colors[x+y*40])]);
      image(font[int(chars[x+40*y])&0xff],x*16+48,y*16+48,16,16);
    }
  }
  public void setColor(int x,int y, int value){
    if(colors[40*y+x]!=byte(value&0xff)){
      colors[40*y+x]=byte(value&0xff);
      tint(palette[backgroundColor]);
      image(empty,x*16+48,y*16+48,16,16);
      tint(palette[int(colors[x+y*40])]);
      image(font[int(chars[x+40*y])&0xff],x*16+48,y*16+48,16,16);
    }
  }
  public void setCell(int x,int y, int cellChar,int cellColor){
    boolean changed=false;
    if(colors[40*y+x]!=byte(cellColor&0xff)){
      colors[40*y+x]=byte(cellColor&0xff);
      tint(palette[this.backgroundColor]);
      changed=true;
    }
    if(chars[40*y+x]!=byte(cellChar&0xff)){
      chars[40*y+x]=byte(cellChar&0xff);
      tint(palette[this.backgroundColor]);
      changed=true;
    }
    if(changed){
      image(empty,x*16+48,y*16+48,16,16);
      tint(palette[int(colors[x+y*40])]);
      image(font[int(chars[x+40*y])&0xff],x*16+48,y*16+48,16,16);
    }
  }
  
  
  private void convertFont(){
    byte[] fontBytes=loadBytes("data/chargen");
    for(int i=0;i<256;i++){
      font[i]=createImage(8,8,ARGB);
      font[i].loadPixels();
      for(int y=0;y<8;y++)
        for(int x=0;x<8;x++){
          if(((fontBytes[i*8+y]>>(7-x))&1)==1)
            font[i].pixels[y*8+x]=color(255);
          else font[i].pixels[y*8+x]=color(0,0,0,0);  
        }
      font[i].updatePixels();
    }   
  }
  
  public void render(int ox,int oy,int charSize){
    noStroke();
    fill(palette[int(borderColor)]);
    rect(ox,oy,46*charSize,32*charSize);
    fill(palette[int(backgroundColor)]);
    rect(ox+3*charSize,oy+3*charSize,40*charSize,25*charSize);
    for(int x=0;x<40;x++)
      for(int y=0;y<25;y++){
        tint(palette[int(colors[x+y*40])]);
        image(font[int(chars[x+y*40])],3*charSize+x*charSize,3*charSize+y*charSize,charSize,charSize);
      }  
  }
  public void loadPic(String fileName){
    byte[] wholeFile;
    if (sketchFile(fileName).exists()){
      wholeFile=loadBytes(fileName);
      for(int i=0;i<1000;i++){
        this.chars[i]=wholeFile[i+85];
        this.colors[i]=wholeFile[i+1085];
      }
      this.borderColor=wholeFile[2085];
      this.backgroundColor=wholeFile[2086];
      this.render(0,0,16);
      tint(0,255,0,255);
      type("     Loaded!    ",752,484);
      downCount=60;
    } else {
      tint(255,0,0,255);
      type(" File not found ",752,484);
      downCount=60;
    }
  }
  public void savePic(String fileName){
    byte[] newProgram=new byte[2087];
    for(int i=0;i<85;i++)
      newProgram[i]=base[i];
    for(int i=0;i<1000;i++){
      newProgram[i+85]=chars[i];
      newProgram[i+1085]=colors[i];
    }
    newProgram[2085]=this.borderColor;
    newProgram[2086]=this.backgroundColor;
    if (sketchFile(fileName).exists())
      sketchFile(fileName).delete();
    saveBytes(fileName,newProgram);
    tint(0,255,0,255);
    type("     Saved!     ",752,484);
    downCount=60;
  }
  public void drawChar(int x,int y,int character, int charColor){
    tint(charColor);
    image(font[character],x<<4,y<<4,16,16);
  }
  public int getColor(int col){
    return palette[col];
  }
  public void drawEmpty(int x,int y, int charColor){
    tint(palette[charColor]);
    image(empty,x*16,y*16,16,16);
  }
  public void setBorder(int i){
    if(i!=this.borderColor){
      this.borderColor=byte(i&0xf);
      this.render(0,0,16);
    }
  }
  public void setBackground(int i){
    if(i!=this.backgroundColor){
      this.backgroundColor=byte(i&0xf);
      this.render(0,0,16);
    }
  }
  public int getBorder(){
    return this.borderColor;
  }
  public int getBackground(){
    return this.backgroundColor;
  }
}
