PETSCIIBRUSH
============

C64 PETSCII painting tool, written in Processing. Exports to RUNable C64
programs.

TODO
----

* SEQ file output
* Support for typing PETSCII
* Undo
* Copy/paste
